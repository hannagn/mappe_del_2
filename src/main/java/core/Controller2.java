package core;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.Optional;

/**
 * The controller class responsible for the Add Patient GUI page (input.fxml)
 */
public class Controller2 {

    @FXML
    AnchorPane patientDetailsAdd;
    @FXML
    private Button cancelInput;
    @FXML
    private Button OKInput;
    @FXML
    private TextField firstNameInput, lastNameInput, sSecNumInput;

    @FXML //When cancel button pushed, the window closes
    public void exitsWindow() {
        Platform.exit();
    }

    /**
     * Method that is executed when user pushes OK on the Add Patient GUI page,
     * uses helper method displayInput to add the patient to patient register
     *
     * @throws IOException
     */
    @FXML
    public void saveNewPatient() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("javafx.fxml"));
        Parent root = loader.load();

        Controller controller = loader.getController();

        Patient patient = new Patient(firstNameInput.getText(), lastNameInput.getText(), sSecNumInput.getText());

        controller.displayInput(patient);
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }

    Patient patientSelected;

    /**
     * A helper method to set the patient info text in text labels
     * @param patient the patient typed into the table
     */
    @FXML
    public void initInfo(Patient patient){
        patientSelected = patient;
        firstNameInput.setText(patientSelected.getFirstName());
        lastNameInput.setText(patientSelected.getLastName());
        sSecNumInput.setText(patientSelected.getSocialSecurityNumber());
    }
}
