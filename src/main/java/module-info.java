module Mappe_del_2.core {
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;

    opens core to javafx.fxml, javafx.graphics;
    exports core;
}