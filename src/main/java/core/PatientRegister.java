package core;

import java.util.ArrayList;
import java.util.List;

/**
 * A class that represents a register of patients at a hospital
 */
public class PatientRegister {

    private ArrayList<Patient> patients;

    /**
     * Constructor with no parametres that sets the patients list and calls the fillWithPatients method
     */
    public PatientRegister() {
        this.patients = new ArrayList<>();
        this.fillWithPatients();
    }

    /**
     * Method that fills patients to the list
     */
    private void fillWithPatients(){
    }

    //Getter
    public List<Patient> getPatients() {
        return this.patients;
    }

    /**
     * A helper method used in the controller
     * @param patient patient
     */
    public void addPatient(Patient patient) {
        this.patients.add(patient);
    }

    @Override
    public String toString() {
        return "\nPatientRegister " +
                patients;
    }
}
