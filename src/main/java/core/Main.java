package core;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;

public class Main extends Application {

    @Override
    public void start(Stage primarystage) throws IOException{

        URL url = getClass().getResource("javafx.fxml");
        try {
            Parent root = FXMLLoader.load(url);
            Scene scene = new Scene(root, 800, 600);

            primarystage.setTitle("Patients: ");
            primarystage.setScene(scene);
            primarystage.show();
        } catch (IOException e){
            e.printStackTrace();
            throw e;
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
