package core;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;


/**
 * The controller class responsible for the main GUI page (javafx.fxml)
 */
public class Controller implements Initializable {

    private PatientRegister patientRegister;
    private ObservableList<Patient> observablePatientList;

    @FXML
    public Label statusLabel;
    @FXML
    private TableView<Patient> patientsTableView;

    @FXML
    private TableColumn<Patient, String> firstNameColumn, lastNameColumn, sSecNumColumn;

    @FXML //Closes the window when user pushes exit in menubar
    public void exitWindow() {
        Platform.exit();
    }

    @FXML //Method to be executed when the user pushes "Add new Patient" in menu bar
    public void addPatient() throws IOException {
            addNewPatient();
    }

    @FXML //Method to be executed when the user pushes "Add Patient" button
    public void addNewPatient() throws IOException{
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("input.fxml"));
            Parent root = loader.load();

            Controller2 controller2 = loader.getController();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Add Patient");
            stage.show();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    @FXML //Method to be executed when the user pushes "Edit selected Patient" in menu bar
    public void editSelectedPatient() throws IOException{
        editPatient();
    }

    /**
     * Method to be executed when the user pushes "Edit Patient" button
     * Uses the helper method to open the "Add patient" window with stored information about selected patient
     */
    @FXML
    public void editPatient() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("input.fxml"));
            Parent root = loader.load();

            Controller2 controller2 = loader.getController();
            controller2.initInfo(patientsTableView.getSelectionModel().getSelectedItem());
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Edit Patient");
            stage.show();
        }catch(IOException e){
            e.printStackTrace();
        }

    }

    /**
     * This method has the same functionality as the method under this (removePatient())
     */
    @FXML //Method to be executed when the user pushes "Delete selected Patient" in menu bar
    public void deleteSelectedPatient(){
        removePatient();
    }

    /**
     * A method that lets the user delete a patient from the patient register
     * The user is required to select a patient, then click remove button or delete patient via menu
     * If the user clicks "OK" on the alert box, the patient is deleted, else if the user clicks "Cancel"
     * the alert box will close, and no patient is deleted
     */
    @FXML //Method to be executed when the user pushes the "Delete Patient" button
    public void removePatient() {

        Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION);
        confirmation.setTitle("Confirmation Dialog");
        confirmation.setHeaderText("Delete patient?");
        confirmation.setContentText("Are you sure you want to delete this patient?");
        Optional<ButtonType> result = confirmation.showAndWait();
        if(result.get() == ButtonType.OK){

            ObservableList<Patient> patientSelected, allPatients;
            allPatients = patientsTableView.getItems();
            patientSelected = patientsTableView.getSelectionModel().getSelectedItems();
            patientSelected.forEach(allPatients::remove);
        }
    }

    /**
     * A method that shows an alert box with "about" information when a user clicks "helps -> about" on the menubar
     */
    @FXML
    public void showAbout() {
        Alert info = new Alert(Alert.AlertType.INFORMATION);
        info.setTitle("Information Dialog");
        info.setHeaderText("Literature Register:");
        info.setContentText("A useful application created by\n"
                              + "(C) Hanna Nordhus\n"
                              + "v0.1 2021-04-26");
        info.showAndWait();
    }

    /**
     * A helper method that updates the observable list, and sets the patients
     */
    private void updateObservableList(){
        this.observablePatientList.setAll(this.patientRegister.getPatients());
    }

    /**
     * A helper method that creates a patient from the details typed in to the GUI,
     * and adds this patient to the register
     */
    public void displayInput(Patient patient){

        this.patientRegister.addPatient(patient);

        this.updateObservableList();
    }

    /**
     * The method being called when a user clicks Import from CSV in the menu
     * Lets the user choose which file to import
     * Adds this text to the table
     */
    @FXML
    public void openCSV() {
        String line = "";
        BufferedReader reader = null;
        FileChooser fileChooser = new FileChooser();
        File selectedFile = fileChooser.showOpenDialog(null);
        try {
            reader = new BufferedReader(new FileReader(selectedFile));
            String[] nextLine;

            while ((line = reader.readLine()) != null) {
                while ((line = reader.readLine()) != null) {
                    nextLine = line.split(";");
                    observablePatientList.add(new Patient(nextLine[0], nextLine[1], nextLine[3]));
                }
            }
        } catch (Exception e) {
            Alert info = new Alert(Alert.AlertType.INFORMATION);
            info.setTitle("OBS!");
            info.setHeaderText("Something went wrong");
            info.setContentText("The chosen file type is not valid.\n" +
                    "Please choose another file by clicking on Select File or cancel the operation");
            info.showAndWait();
            statusLabel.setText("OBS! Something went wrong");
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method to export information from table in csv format
     */
    public void exportCSV() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extensionFilter);
        File file = fileChooser.showSaveDialog(null);
        FileWriter fileWriter = null;
        try{
            fileWriter = new FileWriter(file);
            fileWriter.write("firstName;lastName;socialSecurityNumber");
            for(Patient patient : observablePatientList){
                fileWriter.write(patient.getFirstName()+";"+patient.getLastName()+";"+patient.getSocialSecurityNumber()+"\n");
            }
            statusLabel.setText("All Good");
            fileWriter.close();
        }catch(IOException e){
            statusLabel.setText("Did not work");
            e.printStackTrace();
        }
       }


    /**
     * A method that fills in the patient register when the application starts running
     * @param url default parameter from the initialize method
     * @param resourceBundle default parameter from the initialize method
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        this.patientRegister = new PatientRegister();
        this.statusLabel.setText("Status: good");
        this.firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        this.lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        this.sSecNumColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        this.observablePatientList = FXCollections.observableArrayList(this.patientRegister.getPatients());
        this.patientsTableView.setItems(this.observablePatientList);
    }
}
